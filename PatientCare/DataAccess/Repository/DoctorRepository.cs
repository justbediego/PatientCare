﻿using PatientCare.DataAccess.UnitOfWork;
using PatientCare.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.DataAccess.Repository
{
    public class DoctorRepository : GenericRepository<Doctor>
    {
        public DoctorRepository(PatientCareContext dbContext)
            : base(dbContext)
        {

        }
    }
}
