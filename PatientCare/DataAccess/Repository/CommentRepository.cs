﻿using PatientCare.DataAccess.UnitOfWork;
using PatientCare.Model;
using PatientCare.Model.InteractionEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.DataAccess.Repository
{
    public class CommentRepository : GenericRepository<Comment>
    {
        public CommentRepository(PatientCareContext dbContext)
            : base(dbContext)
        {

        }
    }
}
