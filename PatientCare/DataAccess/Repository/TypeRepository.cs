﻿using PatientCare.DataAccess.UnitOfWork;
using PatientCare.Model;
using PatientCare.Model.InfoEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.DataAccess.Repository
{
    public class TypeRepository : GenericRepository<Type>
    {
        public TypeRepository(PatientCareContext dbContext)
            : base(dbContext)
        {

        }
    }
}
