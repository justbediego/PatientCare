﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.DataAccess.UnitOfWork
{
    public class UnitOfWorkFactory
    {
        public static IUnitOfWork Create(PatientCareContext context)
        {
            return (IUnitOfWork)new UnitOfWork(context);
        }
    }
}
