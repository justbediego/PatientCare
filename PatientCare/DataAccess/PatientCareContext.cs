﻿using PatientCare.Model;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PatientCare.Model.InfoEngine;
using PatientCare.Model.InteractionEngine;

namespace PatientCare.DataAccess
{
    public class PatientCareContext:DbContext
    {
        public PatientCareContext()
        {
            this.Configuration.AutoDetectChangesEnabled = false;
        }
        public DbSet<Country> Countries { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Doctorship> Doctorships { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Type> Types { get; set; }
        public DbSet<Instance> Instances { get; set; }
        public DbSet<ImageData> ImageDatas { get; set; }
        public DbSet<FileData> FileDatas { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Person> People { get; set; }

    }
}
