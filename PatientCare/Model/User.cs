﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model
{
    public class User : Person
    {
        [Index(IsUnique = true)]
        [StringLength(150)]
        public string Username { get; set; }
        [StringLength(30)]
        public string HashedPassword { get; set; }       
        [StringLength(200)]
        [NotMapped]
        public string Password;
    }
}
