﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model
{
    public class FileData:Base
    {
        [StringLength(300)]
        public string Name { get; set; }
        public string Description { get; set; }
        public byte[] Data { get; set; }
    }
}
