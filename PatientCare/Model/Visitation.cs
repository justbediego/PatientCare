﻿using PatientCare.Model.InfoEngine;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model
{
    public class Visitation : Base
    {
        public Guid VisitationType_ID { get; set; }
        [ForeignKey("VisitationType_ID")]
        public virtual VisitationType VisitationType { get; set; }
        public DateTime DateScheduled { get; set; }
        public DateTime? DateVisited { get; set; }

        public Guid PatientDoctorship_ID { get; set; }
        [ForeignKey("PatientDoctorship_ID")]
        public virtual PatientDoctorship PatientDoctorship { get; set; }        
        public virtual List<Instance> Information { get; set; }
        public Visitation()
        {
            Information = new List<Instance>();
        }
    }
}
