﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model
{
    public class Location:Base
    {
        [StringLength(20)]
        public string PostCode { get; set; }
        public Guid? City_ID { get; set; }
        [ForeignKey("City_ID")]
        public virtual City City { get; set; }
        [StringLength(2000)]
        public string Address { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public virtual List<Contact> Contacts { get; set; }

        public Location()
        {
            Contacts = new List<Contact>();
        }
    }
}
