﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model
{
    public abstract class Person : Base
    {
        public DateTime BirthDate { get; set; }
        [StringLength(10)]
        [Index(IsUnique = true)]
        public string NationalCode { get; set; }
        [StringLength(20)]
        public string PassportNumber { get; set; }
        [StringLength(100)]
        public string FirstName { get; set; }
        [StringLength(200)]
        public string LastName { get; set; }
        [StringLength(100)]
        public string FatherName { get; set; }
        public Guid? CityOfBirth_ID { get; set; }
        [ForeignKey("CityOfBirth_ID")]
        public virtual City CityOfBirth { get; set; }
        public Guid? PersonImage_ID { get; set; }
        [ForeignKey("PersonImage_ID")]
        public virtual ImageData PersonImage { get; set; }
        public Guid? Shenasname_ID { get; set; }
        [ForeignKey("Shenasname_ID")]
        public virtual ImageData ShenasnameImage { get; set; }
        public Guid? NationalCard_ID { get; set; }
        [ForeignKey("NationalCard_ID")]
        public virtual ImageData NationalCardImage { get; set; }
        public Guid? InsuranceCard_ID { get; set; }
        [ForeignKey("InsuranceCard_ID")]
        public virtual ImageData InsuranceCard { get; set; }
        public string InsuranceNumber { get; set; }
        public Guid? HomeLocation_ID { get; set;}
        [ForeignKey("HomeLocation_ID")]
        public virtual Location HomeLocation { get; set; }
    }
}
