﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model
{
    public class Province:Base
    {
        public Guid Country_ID { get; set; }
        [ForeignKey("Country_ID")]
        public virtual Country Country { get; set; }
        [StringLength(200)]
        public string Name { get; set; }
        public virtual List<City> Cities { get; set; }
    }
}
