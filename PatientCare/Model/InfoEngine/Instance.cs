﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model.InfoEngine
{
    public class Instance : Base
    {
        public int AssignerUser_ID { get; set; }
        [ForeignKey("AssignerUser_ID")]
        public virtual User AssignerUser { get; set; }
        public int Type_ID { get; set; }
        public virtual Type Type { get; set; }
        public bool IsVisibleToPatient { get; set; }
        public List<InstanceSingleResult> Results { get; set; }
    }
    public class InstanceSingleResult : Base
    {
        [StringLength(1)]
        public string Alphabet { get; set; }
        public double Number { get; set; }
        public int? Category_ID { get; set; }
        [ForeignKey("Category_ID")]
        public virtual BasicTypes.Categorical.Category Category { get; set; }
        public string UnknownCategory { get; set; }
        public int? ImageID { get; set; }
        [ForeignKey("ImageID")]
        public virtual ImageData Image { get; set; }
        public int? FileID { get; set; }
        [ForeignKey("FileID")]
        public virtual FileData File { get; set; }
        public string Text { get; set; }
        public bool Bool { get; set; }
    }
}
