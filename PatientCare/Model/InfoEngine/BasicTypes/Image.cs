﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model.InfoEngine.BasicTypes
{
    public class Image: BasicType
    {
        public int MaxImageSize { get; set; }
    }
}
