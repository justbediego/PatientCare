﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model.InfoEngine.BasicTypes
{
    public class BasicType: Type
    {
        public enum ResultTypes
        {
            SingleResult = 0,
            MutlipleResults = 1,
            MinimumNumberOfResults = 2,
            MaxmimumNumberOfResults = 3,
            MinimumMaximumNumberOfResults = 4,
        }
        [StringLength(50)]
        public string UnitName { get; set; }
        public int MinimumNumberOfResults { get; set; }
        public int MaximumNumberOfResults { get; set; }
    }
}
