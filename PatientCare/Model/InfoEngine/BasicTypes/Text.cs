﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model.InfoEngine.BasicTypes
{
    public class Text: BasicType
    {
        public int MaxTextLength { get; set; }
    }
}
