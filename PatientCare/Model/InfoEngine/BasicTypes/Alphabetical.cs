﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model.InfoEngine.BasicTypes
{
    public class Alphabetical: BasicType
    {
        [StringLength(1)]
        public string MinAlphabet { get; set; }
        [StringLength(1)]
        public string MaxAlphabet { get; set; }
    }
}
