﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model.InfoEngine.BasicTypes
{
    public class Categorical: BasicType
    {
        public class Category : Base
        {
            [StringLength(100)]
            public string Name { get; set; }
            public virtual List<Category> Children { get; set; }
            public int? Parent_ID { get; set; }
            [ForeignKey("Parent_ID")]
            public virtual Category Parent { get; set; }
        }
        public Categorical()
        {
            Categories = new List<Category>();
        }
        public virtual List<Category> Categories { get; set; }
        public bool MustSelectLeaf { get; set; }
    }
}
