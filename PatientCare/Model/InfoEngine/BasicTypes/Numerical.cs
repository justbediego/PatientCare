﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model.InfoEngine.BasicTypes
{
    public class Numerical: BasicType
    {
        public double MinNumber { get; set; }
        public double MaxNumber { get; set; }
    }
}
