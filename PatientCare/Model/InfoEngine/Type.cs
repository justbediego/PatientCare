﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientCare.Model.InfoEngine
{
    public class Type : Base
    {
        public class TypeUse : Base
        {
            public int ParentType_ID { get; set; }
            [ForeignKey("ParentType_ID")]
            public virtual Type ParentType { get; set; }
            public int ChildType_ID { get; set; }
            [ForeignKey("ChildType_ID")]
            public virtual Type ChildType { get; set; }
            public bool IsOptional { get; set; }
        }
        [StringLength(100)]
        [Index(IsUnique = true)]
        public string Name { get; set; }
        public virtual List<TypeUse> Elements { get; set; }
    }
}
