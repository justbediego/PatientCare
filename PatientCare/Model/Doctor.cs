﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model
{
    [Table("Doctors")]
    public class Doctor : User
    {
        public virtual List<Doctorship> DoctorShips { get; set; }
        [Index(IsUnique = true)]
        [StringLength(100)]
        public string MedicalCode { get; set; }
    }
}
