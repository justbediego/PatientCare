﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model
{
    public class PatientDoctorship:Base
    {
        public Guid Doctorship_ID { get; set; }
        [ForeignKey("Doctorship_ID")]
        public virtual Doctorship Doctorship { get; set; }
        public Guid Patient_ID { get; set; }
        [ForeignKey("Patient_ID")]
        public virtual Patient Patient { get; set; }
    }
}
