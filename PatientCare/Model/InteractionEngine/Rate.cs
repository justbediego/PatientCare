﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model.InteractionEngine
{
    public class Rate : Interaction
    {
        [Range(0,5)]
        public int Value { get; set; }
    }
}
