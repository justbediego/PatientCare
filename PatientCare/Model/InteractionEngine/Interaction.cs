﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model.InteractionEngine
{
    public class Interaction:Base
    {
        public int FromUser_ID { get; set; }
        [ForeignKey("FromUser_ID")]
        public virtual User FromUser { get; set; }
    }
}
