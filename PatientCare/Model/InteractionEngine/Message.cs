﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model.InteractionEngine
{
    public class Message:Interaction
    {
        public int? ToUser_ID { get; set; }
        [ForeignKey("ToUser_ID")]
        public virtual User To { get; set; }
        public List<FileData> Attachments { get; set; }
        public string Subject { get; set; }
        public string Text { get; set; }
    }
}
