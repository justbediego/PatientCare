﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model.InteractionEngine
{
    public class Comment: Interaction
    {
        public string Text { get; set; }
    }
}
