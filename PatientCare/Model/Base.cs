﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model
{
    public abstract class Base
    {
        [Key]
        public Guid ID { get; set; }
        public bool IsDeactivated { get; set; }
        public DateTime DateCreated { get; set; }
        public Base()
        {
            DateCreated = DateTime.Now;
        }
    }
}
