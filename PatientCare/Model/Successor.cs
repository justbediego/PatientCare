﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model
{
    [Table("Successors")]
    public class Successor:User
    {
        public Guid Doctorship_ID { get; set; }
        [ForeignKey("Doctorship_ID")]
        public virtual Doctorship DoctorShip { get; set; }
    }
}
