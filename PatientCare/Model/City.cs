﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model
{
    public class City:Base
    {
        public Guid Province_ID { get; set; }
        [ForeignKey("Province_ID")]
        public virtual Province Province { get; set; }
        [StringLength(200)]
        public string Name { get; set; }
    }
}
