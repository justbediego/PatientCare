﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model
{
    public class Patient: User
    {
        public Patient()
        {
            Visitations = new List<Visitation>();
            Doctorships = new List<PatientDoctorship>();
        }
        public virtual List<Visitation> Visitations { get; set; }
        public virtual List<PatientDoctorship> Doctorships { get; set; }
    }
}
