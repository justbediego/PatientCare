﻿using PatientCare.Model.InfoEngine;
using PatientCare.Model.InteractionEngine;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model
{
    public class Doctorship : Base
    {
        public Doctorship()
        {
            Doctors = new List<Doctor>();
            Successors = new List<Successor>();
            Comments = new List<Comment>();
            Rates = new List<Rate>();
            Offices = new List<Location>();
            Patients = new List<PatientDoctorship>();
            VisitationTypes = new List<VisitationType>();
        }
        public string Name { get; set; }
        public virtual List<Doctor> Doctors { get; set; }
        public virtual List<Successor> Successors { get; set; }
        public virtual List<Comment> Comments { get; set; }
        public virtual List<Rate> Rates { get; set; }
        public virtual List<Location> Offices { get; set; }
        public virtual List<PatientDoctorship> Patients { get; set; }
        public virtual List<VisitationType> VisitationTypes { get; set; }
        public virtual List <Type> Types { get; set; }
        public bool PatientsHaveDashboard { get; set; }
        public bool OtherDoctorsCanObserve { get; set; }
        public bool HasComments { get; set; }
        public bool HasRates { get; set; }
    }
}
