﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Model
{
    public class Contact : Base
    {
        public Guid Location_ID { get; set; }
        [ForeignKey("Location_ID")]
        public virtual Location Location { get; set; }
        public enum ContactTypes
        {
            Phone = 1,
            Mobile = 2,
            Email = 3,
            Fax = 4,
        }
        public ContactTypes Type { get; set; }
        [StringLength(200)]
        public string Value { get; set; }
    }
}
