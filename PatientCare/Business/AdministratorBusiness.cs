﻿using PatientCare.DataAccess;
using PatientCare.DataAccess.Repository;
using PatientCare.DataAccess.UnitOfWork;
using PatientCare.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Business
{
    public class AdministratorBusiness
    {
        // not to take it so hard. we need money
        #region Contructors
        private UnitOfWork unitOfWork;
        private CountryRepository countryRepository;
        private ProvinceRepository provinceRepository;
        private CityRepository cityRepository;
        private DoctorRepository doctorRepository;
        private DoctorshipRepository doctorshipRepository;
        private TypeRepository typeRepository;
        private SystemBusiness systemBusiness;
        public AdministratorBusiness(UnitOfWork unitOfWork, CountryRepository countryRepository, ProvinceRepository provinceRepository,CityRepository cityRepository,DoctorRepository doctorRepository,DoctorshipRepository doctorshipRepository, TypeRepository typeRepository, SystemBusiness systemBusiness)
        {
            this.unitOfWork = unitOfWork;
            this.countryRepository = countryRepository;
            this.provinceRepository = provinceRepository;
            this.cityRepository = cityRepository;
            this.doctorRepository = doctorRepository;
            this.doctorshipRepository = doctorshipRepository;
            this.typeRepository = typeRepository;
            this.systemBusiness = systemBusiness;
        }
        #endregion
        #region Country
        public Country InsertCountry(Country model)
        {
            Country country = new Country()
            {
                Name=model.Name,
            };
            countryRepository.Insert(country);
            unitOfWork.Commit();
            return country;
        }
        public Country UpdateCountry(Guid countryID, Country model)
        {
            Country country = countryRepository.Get(countryID);
            country.Name = model.Name;
            countryRepository.Update(country);
            unitOfWork.Commit();
            return country;
        }
        public void DeleteCountry(Guid countryID)
        {
            countryRepository.Delete(countryID);
            unitOfWork.Commit();
        }
        #endregion
        #region Province
        public Province InsertProvince(Guid countryID, Province model)
        {
            Province province = new Province()
            {
                Country_ID = countryID,
                Name = model.Name,
            };
            provinceRepository.Insert(province);
            unitOfWork.Commit();
            return province;
        }
        public Province UpdateProvince(Guid provinceID, Province model)
        {
            Province province = provinceRepository.Get(provinceID);
            province.Name = model.Name;
            provinceRepository.Update(province);
            unitOfWork.Commit();
            return province;
        }
        public void DeleteProvince(Guid provinceID)
        {
            provinceRepository.Delete(provinceID);
            unitOfWork.Commit();
        }
        #endregion
        #region Cities
        public City InsertCity(Guid provinceID, City model)
        {
            City city = new City()
            {
                Province_ID = provinceID,
                Name = model.Name,
            };
            cityRepository.Insert(city);
            unitOfWork.Commit();
            return city;
        }
        public City UpdateCity(Guid cityID, City model)
        {
            City city = cityRepository.Get(cityID);
            city.Name = model.Name;
            cityRepository.Update(city);
            unitOfWork.Commit();
            return city;
        }
        public void DeleteCity(Guid cityID)
        {
            cityRepository.Delete(cityID);
            unitOfWork.Commit();
        }
        #endregion
        #region Doctor
        public Doctor InsertDoctor(Doctor model)
        {
            Doctor doctor = new Doctor()
            {
                BirthDate = model.BirthDate,
                CityOfBirth_ID = model.CityOfBirth_ID,
                FatherName = model.FatherName,
                FirstName = model.FirstName,
                LastName = model.LastName,
                MedicalCode = model.MedicalCode,
                NationalCode = model.NationalCode,
                Username = model.Username,
                HashedPassword = systemBusiness.HosseinHash(model.Password),
            };
            doctorRepository.Insert(doctor);
            unitOfWork.Commit();
            return doctor;
        }
        public Doctor UpdateDoctor(Guid doctorID, Doctor model)
        {
            Doctor doctor = doctorRepository.Get(doctorID);
            doctor.BirthDate = model.BirthDate;
            doctor.CityOfBirth_ID = model.CityOfBirth_ID;
            doctor.FatherName = model.FatherName;
            doctor.FirstName = model.FirstName;
            doctor.LastName = model.LastName;
            doctor.MedicalCode = model.MedicalCode;
            doctor.NationalCode = model.NationalCode;
            doctorRepository.Update(doctor);
            unitOfWork.Commit();
            return doctor;
        }
        public void DeleteDoctor(Guid doctorID)
        {
            doctorRepository.Delete(doctorID);
            unitOfWork.Commit();
        }
        public void AssignDoctorToDoctorship(Guid doctorID, Guid doctorshipID, bool isAssigned)
        {
            Doctor doctor = doctorRepository.Get(doctorID);
            Doctorship doctorship = doctorshipRepository.Get(doctorshipID);
            if (doctorship.Doctors.Contains(doctor))
            {
                if (!isAssigned)
                {
                    doctorship.Doctors.Remove(doctor);
                }
            }
            else
            {
                if (isAssigned)
                {
                    doctorship.Doctors.Add(doctor);
                }
            }
            doctorshipRepository.Update(doctorship);
            unitOfWork.Commit();
        }
        #endregion
        #region DoctorShip
        public Doctorship InsertDoctorShip(Doctorship model)
        {
            Doctorship doctorship = new Doctorship()
            {
                HasComments= model.HasComments,
                HasRates= model.HasRates,
                Name= model.Name,
                PatientsHaveDashboard= model.PatientsHaveDashboard,
            };
            doctorshipRepository.Insert(doctorship);
            unitOfWork.Commit();
            return doctorship;
        }
        public Doctorship UpdateDoctorShip(Guid doctorshipID, Doctorship model)
        {
            Doctorship doctorship = doctorshipRepository.Get(doctorshipID);
            doctorship.HasComments = model.HasComments;
            doctorship.HasRates = model.HasRates;
            doctorship.Name = model.Name;
            doctorship.PatientsHaveDashboard = model.PatientsHaveDashboard;
            doctorshipRepository.Update(doctorship);
            unitOfWork.Commit();
            return doctorship;
        }
        public void DeleteDoctorShip(Guid doctorshipID)
        {
            doctorshipRepository.Delete(doctorshipID);
            unitOfWork.Commit();
        }
        #endregion
        //#region InfoType
        //public InfoType InsertInfoType(InfoType model)
        //{
        //    InfoType infoType = new InfoType()
        //    {
        //        HasMultipleResults = model.HasMultipleResults,
        //        Name = model.Name,
        //        UnitName = model.UnitName,
        //    };
        //    if (model is AlphabeticalInfoType)
        //    {
        //        AlphabeticalInfoType alphabeticalInfoType = (AlphabeticalInfoType)infoType;
        //        alphabeticalInfoType.MinAlphabet = ((AlphabeticalInfoType)model).MinAlphabet;
        //        alphabeticalInfoType.MaxAlphabet = ((AlphabeticalInfoType)model).MaxAlphabet;
        //    }
        //    else if (model is CategoricalInfoType)
        //    {
        //        CategoricalInfoType categoricalInfoType = (CategoricalInfoType)infoType;
        //        categoricalInfoType.CanSelectUnknownCategory = ((CategoricalInfoType)model).CanSelectUnknownCategory;
        //        categoricalInfoType.MustSelectLeaf = ((CategoricalInfoType)model).MustSelectLeaf;
        //    }
        //    else if (model is FileInfoType) {
        //        FileInfoType fileInfoType = (FileInfoType)infoType;
        //        fileInfoType.MaxFileSize = ((FileInfoType)model).MaxFileSize;
        //    }
        //    else if (model is ImageInfoType) {
        //        ImageInfoType imageInfoType = (ImageInfoType)infoType;
        //        imageInfoType.MaxImageSize = ((ImageInfoType)model).MaxImageSize;
        //    }
        //    else if (model is NumericalInfoType) {
        //        NumericalInfoType numericalInfoType = (NumericalInfoType)infoType;
        //        numericalInfoType.MinNumber = ((NumericalInfoType)model).MinNumber;
        //        numericalInfoType.MaxNumber = ((NumericalInfoType)model).MaxNumber;
        //    }
        //    else if (model is TextInfoType) {
        //        TextInfoType textInfoType = (TextInfoType)infoType;
        //        textInfoType.MaxTextLength = ((TextInfoType)model).MaxTextLength;
        //    }
        //    else if (model is BooleanInfoType) {
        //        BooleanInfoType booleanInfoType = (BooleanInfoType)infoType;
        //    }
        //    else
        //        throw new Exception.UnknownOperationException("Unknown InfoType");
        //    infoTypeRepository.Insert(infoType);
        //    unitOfWork.Commit();
        //    return infoType;
        //}
        //public InfoType UpdateInfoType(int infoTypeID, InfoType model)
        //{
        //    InfoType infoType = infoTypeRepository.Get(infoTypeID);
        //    infoType.HasMultipleResults = model.HasMultipleResults;
        //    infoType.Name = model.Name;
        //    infoType.UnitName = model.UnitName;
        //    if (model is AlphabeticalInfoType)
        //    {
        //        AlphabeticalInfoType alphabeticalInfoType = (AlphabeticalInfoType)infoType;
        //        alphabeticalInfoType.MinAlphabet = ((AlphabeticalInfoType)model).MinAlphabet;
        //        alphabeticalInfoType.MaxAlphabet = ((AlphabeticalInfoType)model).MaxAlphabet;
        //    }
        //    else if (model is CategoricalInfoType)
        //    {
        //        CategoricalInfoType categoricalInfoType = (CategoricalInfoType)infoType;
        //        categoricalInfoType.CanSelectUnknownCategory = ((CategoricalInfoType)model).CanSelectUnknownCategory;
        //        categoricalInfoType.MustSelectLeaf = ((CategoricalInfoType)model).MustSelectLeaf;
        //    }
        //    else if (model is FileInfoType)
        //    {
        //        FileInfoType fileInfoType = (FileInfoType)infoType;
        //        fileInfoType.MaxFileSize = ((FileInfoType)model).MaxFileSize;
        //    }
        //    else if (model is ImageInfoType)
        //    {
        //        ImageInfoType imageInfoType = (ImageInfoType)infoType;
        //        imageInfoType.MaxImageSize = ((ImageInfoType)model).MaxImageSize;
        //    }
        //    else if (model is NumericalInfoType)
        //    {
        //        NumericalInfoType numericalInfoType = (NumericalInfoType)infoType;
        //        numericalInfoType.MinNumber = ((NumericalInfoType)model).MinNumber;
        //        numericalInfoType.MaxNumber = ((NumericalInfoType)model).MaxNumber;
        //    }
        //    else if (model is TextInfoType)
        //    {
        //        TextInfoType textInfoType = (TextInfoType)infoType;
        //        textInfoType.MaxTextLength = ((TextInfoType)model).MaxTextLength;
        //    }
        //    else if (model is BooleanInfoType)
        //    {
        //        BooleanInfoType booleanInfoType = (BooleanInfoType)infoType;
        //    }
        //    else
        //        throw new Exception.UnknownOperationException("Unknown InfoType");
        //    infoTypeRepository.Update(infoType);
        //    unitOfWork.Commit();
        //    return infoType;
        //}
        //public void DeleteInfoType(int infoTypeID)
        //{
        //    infoTypeRepository.Delete(infoTypeID);
        //    unitOfWork.Commit();
        //}

        //#endregion
    }
}
