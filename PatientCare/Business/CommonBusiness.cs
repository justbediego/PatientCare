﻿using PatientCare.DataAccess;
using PatientCare.DataAccess.Repository;
using PatientCare.DataAccess.UnitOfWork;
using PatientCare.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Business
{
    public class CommonBusiness
    {
        #region Contructors
        private UnitOfWork unitOfWork;
        private CountryRepository countryRepository;
        private ProvinceRepository provinceRepository;
        private CityRepository cityRepository;
        private DoctorRepository doctorRepository;
        private DoctorshipRepository doctorshipRepository;
        private TypeRepository typeRepository;
        private SystemBusiness systemBusiness;
        public CommonBusiness(UnitOfWork unitOfWork, CountryRepository countryRepository, ProvinceRepository provinceRepository, CityRepository cityRepository, DoctorRepository doctorRepository, DoctorshipRepository doctorshipRepository, TypeRepository typeRepository, SystemBusiness systemBusiness)
        {
            this.unitOfWork = unitOfWork;
            this.countryRepository = countryRepository;
            this.provinceRepository = provinceRepository;
            this.cityRepository = cityRepository;
            this.doctorRepository = doctorRepository;
            this.doctorshipRepository = doctorshipRepository;
            this.typeRepository = typeRepository;
            this.systemBusiness = systemBusiness;
        }
        #endregion
        #region Countries
        public List<Country> GetAllCountries()
        {
            int count;
            return countryRepository.GetAll(out count);
        }
        #endregion
        #region Provinces


        #endregion
    }
}
