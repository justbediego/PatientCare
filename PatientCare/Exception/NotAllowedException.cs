﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Exception
{
    public class NotAllowedException : System.Exception
    {
        public NotAllowedException(string source)
            : base(string.Format("ERROR MASH02: Accessing '{0}' is not allowed", source))
        {

        }
    }
}
