﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Exception
{
    public class UnknownOperationException : System.Exception
    {
        public UnknownOperationException(string source)
            : base(string.Format("ERROR MASH03: Unknown Operation Exception: '{0}'", source))
        {

        }
    }
}
