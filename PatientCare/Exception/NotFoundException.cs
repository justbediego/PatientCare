﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCare.Exception
{
    public class NotFoundException : System.Exception
    {
        public NotFoundException(string source)
            : base(string.Format("ERROR MASH01: '{0}' was not found", source))
        {

        }
    }
}
