
(function () {
    angular.module('PatientCare', [
        'ui.router',                    // Routing
        'oc.lazyLoad',                  // ocLazyLoad
        'ui.bootstrap',                // Angular Translate
        'ngIdle',                       // Idle timer
        'ngSanitize',                    // ngSanitize
        'toaster'
    ])
})();

// Other libraries are loaded dynamically in the config.js file using the library ocLazyLoad