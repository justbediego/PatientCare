﻿function regexInput() {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, mCtrl) {
            function myValidation(value) {
                var regex = new RegExp(attr.regexInput);
                mCtrl.$setValidity('regex', regex.test(value));                
                return value;
            }
            mCtrl.$formatters.push(myValidation);
            mCtrl.$parsers.push(myValidation);
        }
    };
}

angular
    .module('PatientCare')
    .directive('regexInput', regexInput);
