﻿function adminCountriesCtrl($scope, $uibModal, $http, toaster) {
    $scope.allCountries = [];
    $scope.openNewCountryModal = function () {
        $uibModal.open({
            templateUrl: 'openNewCountryTemplate',
            controller: 'adminNewCountryCtrl',
        }).closed.then(function () {
            $scope.loadContries();
        });
    }
    $scope.loadContries = function () {
        $http.get('/Api/Common/GetAllCountries').then(
            function success(response) {
                $scope.allCountries = response.data;
            },
            function error(response) {
                toaster.error('خطا در بارگذاری', response.data.Message);
            }
        );
    };    
    $scope.deleteCountry = function (id) {
        $http.post('/Api/Admin/DeleteCountry', {ID: id}).then(
            function success(response) {
                toaster.success('حذف گردید');
                $scope.loadContries();
            },
            function error(response) {
                toaster.error('خطا در حذف', response.data.Message);
            }
        );
    }
    //Init
    $scope.loadContries();
};

function adminNewCountryCtrl($scope, $http, $uibModalInstance) {
    $scope.data = {
        Name: ''
    };
    $scope.insertNewCountry = function () {
        $http.post('/Api/Admin/InsertCountry', { Name: $scope.data.Name }).then(
            function success(response) {
                $uibModalInstance.close();
            },
            function error(response) {
                toaster.error('خطا در ایجاد', response.data.Message);
            }
        );
    }
}

angular
    .module('PatientCare')
    .controller('adminCountriesCtrl', adminCountriesCtrl)
    .controller('adminNewCountryCtrl', adminNewCountryCtrl);