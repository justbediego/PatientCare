
function config($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, IdleProvider, KeepaliveProvider) {

    // Configure Idle settings
    IdleProvider.idle(5); // in seconds
    IdleProvider.timeout(120); // in seconds

    $urlRouterProvider.otherwise("/");

    $ocLazyLoadProvider.config({
        debug: false,
    });
    $stateProvider
        .state('admin', {
            abstract: true,
            url: "/Admin",
            templateUrl: "views/common/content.html",
        })
        .state('admin.countries', {
            url: "/Countries",
            templateUrl: "/views/admin/countries.html",
            controller: 'adminCountriesCtrl',
            //resolve: {
            //    loadPlugin: function ($ocLazyLoad) {
            //        return $ocLazyLoad.load([

            //        ]);
            //    }
            //}
        })
        .state('admin.provinces', {
            url: "/Provinces",
            templateUrl: "/views/admin/provinces.html",
            controller: 'adminProvincesCtrl'
        })
        .state('admin.cities', {
            url: "/Cities",
            templateUrl: "/views/admin/cities.html",
            controller: 'adminCitiesCtrl'
        })
    ;
}
angular
    .module('PatientCare')
    .config(config)
    .run(function ($rootScope, $state) {
        $rootScope.$state = $state;
    });
