﻿using Autofac;
using Autofac.Integration.WebApi;
using PatientCare.Business;
using PatientCare.DataAccess;
using PatientCare.DataAccess.Repository;
using PatientCare.DataAccess.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Web.SessionState;

namespace PatientCare.UI
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            GlobalConfiguration.Configure(Api.Config.Register);
            Autofac();
        }
        private void Autofac()
        {
            var builder = new ContainerBuilder();
            var config = GlobalConfiguration.Configuration;
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            //Basics
            builder.RegisterType<PatientCareContext>().InstancePerRequest();
            builder.RegisterType<UnitOfWork>().InstancePerRequest();
            //Repositories
            builder.RegisterType<CountryRepository>().InstancePerRequest();
            builder.RegisterType<ProvinceRepository>().InstancePerRequest();
            builder.RegisterType<CityRepository>().InstancePerRequest();
            builder.RegisterType<DoctorRepository>().InstancePerRequest();
            builder.RegisterType<DoctorshipRepository>().InstancePerRequest();
            builder.RegisterType<TypeRepository>().InstancePerRequest();
            //Businesses
            builder.RegisterType<SystemBusiness>().InstancePerRequest();
            builder.RegisterType<AdministratorBusiness>().InstancePerRequest();
            builder.RegisterType<CommonBusiness>().InstancePerRequest();
            //
            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}