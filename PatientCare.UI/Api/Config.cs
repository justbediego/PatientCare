﻿using System.Web.Http;

namespace PatientCare.UI.Api
{
    public static class Config
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}",
                defaults: new {  }
            );
        }
    }
}
