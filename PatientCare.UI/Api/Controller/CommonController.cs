﻿using PatientCare.Business;
using PatientCare.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PatientCare.UI.Api.Controller
{
    public class CommonController : ApiController
    {
        private readonly CommonBusiness commonBusiness;
        public CommonController(CommonBusiness commonBusiness)
        {
            this.commonBusiness = commonBusiness;
        }
        public List<Country> GetAllCountries()
        {
            return commonBusiness.GetAllCountries();
        }
    }
}
