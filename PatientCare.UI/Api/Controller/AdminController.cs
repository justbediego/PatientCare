﻿using PatientCare.Business;
using PatientCare.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PatientCare.UI.Api.Controller
{
    public class AdminController : ApiController
    {
        private readonly AdministratorBusiness administratorBusiness;
        public AdminController(AdministratorBusiness administratorBusiness)
        {
            this.administratorBusiness = administratorBusiness;
        }
        [HttpPost]
        public Country InsertCountry(Country model)
        {
            return administratorBusiness.InsertCountry(model);
        }
        [HttpPost]
        public Country UpdateCountry(Country model)
        {
            return administratorBusiness.UpdateCountry(model.ID, model);
        }
        [HttpPost]
        public void DeleteCountry(Country model)
        {
            administratorBusiness.DeleteCountry(model.ID);
        }
    }
}
